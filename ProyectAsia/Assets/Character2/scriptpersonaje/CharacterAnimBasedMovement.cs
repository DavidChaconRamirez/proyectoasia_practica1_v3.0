﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class CharacterAnimBasedMovement : MonoBehaviour
{
    public float rotationSpeed = 4f;
    public float rotationThreshold = 0.3f;
    [Range(0, 180f)]
    public float degreesToTurn = 160f;

    private bool idleIsChanged = false;
    private float timeToWait = 0;
    private float timeToChange = 0;
    private float MIN_WAITING_TIME = 15f;
    private float MAX_WAITING_TIME = 45f;
    private float lastKeyEnteredTimeAt = 0;

    [Header("Animator Parameters")]
    public string motionParam = "motion";
    public string mirrorIdleParam = "mirrorIdle";
	public string IdleType = "Idle_type";
    public string turn180Param = "turn180";
    public string jumping = "Jump";
    public string STAIR = "Stair";
    public string STAIRDOWN = "StairDown";

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    private Ray wallRay = new Ray();
    private float Speed;
    private Vector3 desiredMoveDirection;
    private CharacterController characterController;
    private Animator animator;
    private bool mirrorIdle;
    private bool turn180;
    private int idleRandom = 0;
    private float floatrandom = 0;
    private bool SubirE = false;
    private bool BajarE = false;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    public void moveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash){

        Debug.Log(Speed);
	Speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

    if (Input.anyKey)
    {
      lastKeyEnteredTimeAt = Time.time;
    }

    
        if (Input.GetButtonDown("Jump") && this.animator.GetCurrentAnimatorStateInfo(0).IsName("Jump") != true)
        {
		animator.SetBool(jumping, true);
	    } else if (this.animator.GetCurrentAnimatorStateInfo(0).IsName("Jump") != false) {
		animator.SetBool(jumping, false);
	    }

	if(Speed >= Speed - rotationThreshold && dash){
		Speed = 1.5f;
	}

        if(Speed > rotationThreshold){
		animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
		Vector3 forward = cam.transform.forward;
		Vector3 right = cam.transform.right;

		forward.y = 0f;
		right.Normalize();

		desiredMoveDirection = forward * vInput + right * hInput;
            if (Vector3.Angle(transform.forward, desiredMoveDirection) >= degreesToTurn) {
                turn180 = true;
            
		}else{
			turn180 = false;
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed * Time.deltaTime);
		}
		animator.SetBool(turn180Param, turn180);
		animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
		
	}else if (Speed < rotationThreshold){
            animator.SetBool(mirrorIdleParam, mirrorIdle);
		    animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
            if (Time.time - lastKeyEnteredTimeAt >= MIN_WAITING_TIME && !idleIsChanged)
            {
                idleIsChanged = true;
                timeToWait = UnityEngine.Random.Range(MIN_WAITING_TIME, MAX_WAITING_TIME);
                timeToChange = timeToWait - MIN_WAITING_TIME;
                Invoke("SwitchIdleRandomTime", timeToChange);
            }
            else
            {
                if (idleIsChanged && (Time.time - (lastKeyEnteredTimeAt + timeToWait) >= MIN_WAITING_TIME))
                {
                    lastKeyEnteredTimeAt = Time.time - MIN_WAITING_TIME;
                    idleIsChanged = false;
                }
            }
        }
    }

    private void OnAnimatorIK(int layerIndex){
	if (Speed < rotationThreshold) return;

	float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
	float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

	if (distanceToRightFoot > distanceToLeftFoot){
		mirrorIdle = true;
	} else {
		mirrorIdle = false;
	}
    }

    private void SwitchIdleRandomTime()
    {
        //Aplica un idle aleatorio
        idleRandom = UnityEngine.Random.Range(1, 4);
        floatrandom = (float)idleRandom;
        animator.SetFloat(IdleType, floatrandom);
    }

    void OnTriggerEnter(Collider collision)
    {
        //animator.SetBool(STAIR, true);
        if (collision.tag == "SubirE")
        {
            SubirE = true;
            BajarE = false;
            animator.SetBool(STAIR, false);
            animator.SetBool(STAIRDOWN, false);
        }
        /*if (collision.tag == "Stair" && pitter = true)
        {
            animator.SetBool(STAIR, true);
        }*/
        if (collision.tag == "BajarE")
        {
            SubirE = false;
            BajarE = true;
            animator.SetBool(STAIR, false);
            animator.SetBool(STAIRDOWN, false);
        }
    }

    void OnTriggerStay(Collider collision)
    {

        if (SubirE == true && collision.tag == "Stair" && BajarE == false)
        {
            animator.SetBool(STAIR, true);
            animator.SetBool(STAIRDOWN, false);
        }

        if (BajarE == true && collision.tag == "Stair" && SubirE == false)
        {
            animator.SetBool(STAIR, false);
            animator.SetBool(STAIRDOWN, true);
        }
    }

    void OnTriggerExit(Collider collision)
    {

        if (collision.tag == "Stair")
        {
            animator.SetBool(STAIR, false);
            animator.SetBool(STAIRDOWN, false);
        }
    }


}